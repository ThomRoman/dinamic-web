# INSTALLATION

```shell
  > git clone https://github.com/MaurickThom/Starter-Kit.git ~/Desktop/[something] && cd ~/Desktop/[something]
  > npm i
  > npm start
```

## Eliminating Unused CSS

- [https://survivejs.com/webpack/styling/eliminating-unused-css/](https://survivejs.com/webpack/styling/eliminating-unused-css/)
- [https://github.com/FullHuman/purgecss](https://github.com/FullHuman/purgecss)
- [https://github.com/FullHuman/purgecss/tree/master/packages/purgecss-webpack-plugin](https://github.com/FullHuman/purgecss/tree/master/packages/purgecss-webpack-plugin)
- [https://github.com/webpack-contrib/purifycss-webpack](https://github.com/webpack-contrib/purifycss-webpack)
- [https://github.com/uncss/uncss](https://github.com/uncss/uncss)
- [https://www.campusmvp.es/recursos/post/postcss-como-reducir-hojas-de-estilo-css-eliminando-los-selectores-que-sobran.aspx](https://www.campusmvp.es/recursos/post/postcss-como-reducir-hojas-de-estilo-css-eliminando-los-selectores-que-sobran.aspx)
- [https://www.youtube.com/watch?v=TSQE_ANDVtQ](https://www.youtube.com/watch?v=TSQE_ANDVtQ)

## Betters plugins

- [https://linguinecode.com/post/top-webpack-plugins](https://linguinecode.com/post/top-webpack-plugins)

## [Proyecto De prueba](https://dinamic-web.web.app/)
