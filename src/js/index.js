import * as firebase from 'firebase';
import {firebaseConfig} from './modules/config'

import OpenMenu from './modules/menu';
import { signOut,signIn } from './modules/auth';
import { deletePhotos,savePhotos,showPhotos } from './modules/photos';
import {
  ABOUT_TEMPLATE,
  ADMIN_TEMPLATE,
  CONTACT_TEMPLATE,
  HOME_TEMPLATE,
  ADMIN_AUTH_TEMPLATE} from './tpls/main.tpl'

OpenMenu();
// AuthenticationFirebase();

  // Initialize Firebase
firebase.initializeApp(firebaseConfig);
firebase.analytics();

const main = document.querySelector('.main'),
  footerYear = document.querySelector('.header__footer-year')

footerYear.textContent = new Date().getFullYear()

document.addEventListener('DOMContentLoaded', e => {
  main.innerHTML = HOME_TEMPLATE
  showPhotos()
})
document.addEventListener('click', e => {
  if (e.target.matches('a[href="#"]')) {
    e.preventDefault()
    header.classList.remove("is-active");
    main.classList.remove('is-active')
    document.querySelector('.hamburger').classList.remove('is-active')
  }
  if (e.target.matches('#home')) {
    main.innerHTML = HOME_TEMPLATE
    showPhotos()
  } else if (e.target.matches('#about')) {
    main.innerHTML = ABOUT_TEMPLATE
    document.querySelector('.about-header').innerHTML = `
      <img src="https://i.picsum.photos/id/237/500/500.jpg">
      <img src="https://i.picsum.photos/id/10/500/500.jpg">
    `
  } else if (e.target.matches('#contact')) {
    main.innerHTML = CONTACT_TEMPLATE
  } else if (e.target.matches('#admin')) {
    firebase.auth().onAuthStateChanged(user => {
      if (user) {
        main.innerHTML = ADMIN_AUTH_TEMPLATE
        document.querySelector('.admin-name').textContent = user.displayName
        document.querySelector('.admin-avatar').src = user.photoURL
        savePhotos()
        showPhotos()
      } else {
        main.innerHTML = ADMIN_TEMPLATE
      }

    })
  } else if(e.target.matches('#login')){
    signIn()
  }else if(e.target.matches('#logout')){
    signOut()
  } else if(e.target.closest('.delete-photo') !== null){
    // console.log();
    deletePhotos(
      e.target.closest('.delete-photo').querySelector('.fa-trash').dataset.photo,
      e.target.closest('.delete-photo').querySelector('.fa-trash').dataset.id)
  }
})


document.addEventListener('change',event=>{
  if(event.target.matches('input[type="file')){
    document.querySelector('.form-uploader input[type="text').value = event.target.value
  }
})
