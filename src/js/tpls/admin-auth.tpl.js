export const ADMIN_AUTH_TEMPLATE = /*html*/`
<section class="admin-auth">
<aside class="admin-user">
  <h2 class="admin-name"></h2>
  <img class="admin-avatar">
  <button id="logout" class="sign-btn">
    <i class="fas fa-sign-out-alt"></i>
    salir
  </button>
</aside>
<form id="form-save-photo" class="form">
  <h2>Sube una foto</h2>
  <div class="form-uploader">
    <input type="file" name="photo" required>
    <input type="text" placeholder="Sube tu foto" disabled>
    <span style="cursor:pointer;">
      <i class="fas fa-camera fa-lg" style="cursor:pointer;"></i>
    </span>
  </div>
  <input type="text" name="title" placeholder="Titulo" required>
  <progress class="u-none" value="0" max="100" id="progress-bar"></progress>
  <span id="progress-advance" class="u-none"></span>
  <input type="submit" value="Enviar">
  <p class="u-message"></p>
  </form>
</section>
<article id="photos" class="photos"></article>
`
