export const ADMIN_TEMPLATE = /*html*/`
<section class="admin">
<p class="u-message">Inicia sesión para agregar fotos.</p>
<button id="login" class="sign-btn">
  <i class="fas fa-sign-in-alt"></i>
  entra con
  <i class="fab fa-github"></i>
</button>
</section>
`
