export const CONTACT_TEMPLATE = /*html*/`
  <!--
  https://formspree.io/
  https://www.jotform.com/
-->
<section class="contact">
<form action="https://formspree.io/xpzydzrn" method="POST" class="form">
  <legend class="u-message">Envíame tus comentarios</legend>
  <input type="text" name="name" placeholder="Escribe tu nombre" title="tu nombre" pattern="^[A-Za-zÑñÁáÉéÍíÓóÚúÜü\s]+$"
    required>
  <input type="email" name="email" placeholder="Escribe tu email" title="tu email" pattern="^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$"
    required>
  <input type="text" name="subject" title="asunto a tratar" placeholder="Asunto a tratar" required>
  <textarea name="comments" title="tus comentarios" placeholder="Escribe tus comentarios" required></textarea>
  <input type="submit" value="Enviar">
</form>
</section>
`
