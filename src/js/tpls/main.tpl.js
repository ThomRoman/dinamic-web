import {ABOUT_TEMPLATE} from './about.tpl'
import {ADMIN_TEMPLATE} from './admin.tpl'
import {CONTACT_TEMPLATE} from './contact.tpl'
import {HOME_TEMPLATE} from './home.tpl'
import {ADMIN_AUTH_TEMPLATE} from './admin-auth.tpl'

export {
  ABOUT_TEMPLATE,
  ADMIN_TEMPLATE,
  CONTACT_TEMPLATE,
  HOME_TEMPLATE,
  ADMIN_AUTH_TEMPLATE
}
