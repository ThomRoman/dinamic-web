export default function () {
  const iconMenu = document.getElementById("icon-menu");
  const header = document.getElementById("header");
  const main = document.getElementById('main')
  if(!iconMenu || !header || !main)
    return;
  function toggleMenu() {
    header.classList.toggle("is-active");
    document.querySelector('.hamburger').classList.toggle('is-active')
    main.classList.toggle('is-active')

  }
  function removeMenu() {
    header.classList.remove("is-active");
    main.classList.remove('is-active')
    document.querySelector('.hamburger').classList.remove('is-active')
  }

  const mediumBp = matchMedia("(min-width:45rem)");
  function closeMenu(mediaQueryList) {
    mediaQueryList.matches ? removeMenu() : null;
  }

  closeMenu(mediumBp);
  mediumBp.addListener(closeMenu);
  iconMenu.addEventListener("click", toggleMenu);
}
