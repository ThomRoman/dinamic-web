import firebase from 'firebase/app'
import 'firebase/auth'
import 'firebase/database'
import 'firebase/storage'



const saveInDB = (uid,title,photo) =>{
  let db = firebase.database().ref().child('photos')
  db.push({
    uid,
    title,
    photo
  })
}
function figureTemplate(key,{ uid,title,photo }){

  let _auth = firebase.auth().currentUser != null ?
  /*html*/`
  <div class="delete-photo">
    <i class="fas fa-trash" data-id='${key}' data-photo='${photo}'></i>
  </div>` : ''

  return `
      <img src="${photo}" alt="${title}">
      <figcaption>
        <span>${title}</span>
        ${_auth}
      </figcaption>
  `
}



const uploadPhoto = photo => {
  let photoName = `${new Date().getTime()}_${photo.name}`
  const photosRef = firebase.storage().ref().child('photos'),
    progressBar = document.getElementById('progress-bar'),
    progressAdvance = document.getElementById('progress-advance')

  let uploader = photosRef.child(photoName).put(photo)
  progressBar.classList.remove('u-none')
  progressAdvance.classList.remove('u-none')

  uploader.on(
    'state_changed',
    data=>{
      let progress = Math.floor((data.bytesTransferred / data.totalBytes) * 100)
      progressBar.value = progress
      progressAdvance.textContent = `${progress}`
    },
    err=>progressAdvance.innerHTML = /*html*/`<p class="u-message"> ${err.code} - ${err.message}</p>`,
    ()=>{
      const photoURL = photosRef.child(photoName),
      uid = firebase.auth().currentUser.uid,
      title = document.querySelector('input[name="title"]').value

      uploader = null
      photoURL.getDownloadURL()
        .then(url=>saveInDB(uid,title,url))
        .then(()=>{
          progressBar.value = 0
          progressAdvance.classList.add('u-none')
          progressAdvance.textContent = null
          progressBar.classList.add('u-none')
          document.getElementById('form-save-photo').reset()
        })
    }
  )
}

export const deletePhotos = (photo,uid) => {
  const db = firebase.database(),
    storage = firebase.storage(),
    photos = document.getElementById('photos')

  let isDelete = confirm('¿Estas seguro de eliminar la foto?')

  if(!isDelete) return
  storage.refFromURL(photo).delete()
    .then( ()=>db.ref(`photos/${uid}`).remove() )
  db.ref().child('photos').on('child_removed',data=>{
    let toDelete = document.getElementById(data.key)
    photos.removeChild(toDelete)
  })

}
export function savePhotos (){

  document.addEventListener('submit',event=>{
    event.preventDefault();
    const target = event.target;
    if( target.matches('#form-save-photo') ){
      let photo =  target.photo.files[0]

      if(!photo.type.match('image.*')){
        return target.querySelector('.u-message').innerHTML = /*html*/`
        El archivo que intentas subir no es una imagen<br><i class="fas fa-sad-cry"></i>`
      }
      uploadPhoto(photo)
    }
  })
}

export const showPhotos = () => {
  const db = firebase.database()
  const photos = document.getElementById('photos')

  db.ref().child('photos').on('child_added',data=>{
    let figure = document.createElement('figure')
    figure.id = data.key
    figure.innerHTML = figureTemplate(data.key,data.val())
    // photos.appendChild(figure)
    photos.insertAdjacentElement('afterbegin',figure)
  })
}
