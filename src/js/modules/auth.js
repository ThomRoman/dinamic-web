import firebase from 'firebase/app'
const provider = new firebase.auth.GithubAuthProvider()
export function signIn(){
  return firebase.auth().signInWithPopup(provider)
}
export function signOut() {
  return firebase.auth().signOut()
}
